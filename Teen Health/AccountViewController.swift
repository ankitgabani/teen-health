//
//  AccountViewController.swift
//  Teen Health
//
//  Created by Gabani King on 16/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import WebKit
import SVProgressHUD
import GoogleMobileAds

class AccountViewController: UIViewController,GADBannerViewDelegate {
    
    @IBOutlet weak var txtFirstNam: UITextField!
    @IBOutlet weak var txtLastNAme: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var txtDOb: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBanner()
        
        if let name = UserDefaults.standard.value(forKey: "firstName") as? String {
            txtFirstNam.text = name
        }
        if let lastname = UserDefaults.standard.value(forKey: "lastname") as? String {
            txtLastNAme.text = lastname
            
        }
        if let dob = UserDefaults.standard.value(forKey: "dob") as? String {
            txtDOb.text = dob
            
        }
        if let email = UserDefaults.standard.value(forKey: "email") as? String {
            txtEmail.text = email
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    func addBanner() {
        
        bannerView.delegate = self
        bannerView.rootViewController = self
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.load(GADRequest())
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as! [String]
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                
                UserDefaults.standard.setValue("", forKey: "firstName")
                UserDefaults.standard.setValue("", forKey: "lastname")
                UserDefaults.standard.setValue("", forKey: "dob")
                UserDefaults.standard.setValue("", forKey: "email")
                UserDefaults.standard.set(false, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.window?.rootViewController = homeNavigation
                    appDelegate.window?.makeKeyAndVisible()
                }
                
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } else {
            UserDefaults.standard.setValue("", forKey: "firstName")
                          UserDefaults.standard.setValue("", forKey: "lastname")
                          UserDefaults.standard.setValue("", forKey: "dob")
                          UserDefaults.standard.setValue("", forKey: "email")
                          UserDefaults.standard.set(false, forKey: "UserLogin")
                          UserDefaults.standard.synchronize()
                          
                          let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                          let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
                          let homeNavigation = UINavigationController(rootViewController: home)
                          homeNavigation.navigationBar.isHidden = true
                          if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                              appDelegate.window?.rootViewController = homeNavigation
                              appDelegate.window?.makeKeyAndVisible()
                          }
        }
    }
    
}
