//
//  SideMenuViewController.swift
//  Teen Health
//
//  Created by Gabani King on 16/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController
import GoogleMobileAds

class SideMenuViewController: UIViewController,GADBannerViewDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBanner()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedHome(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigation.viewControllers = [contactViewController]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    
    @IBAction func clickedChecker(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "TeenViewController") as! TeenViewController
        contactViewController.objValue = "Checker"
        navigation.viewControllers = [contactViewController]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    
    @IBAction func clickedFoodGuide(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "TeenViewController") as! TeenViewController
        contactViewController.objValue = "Food"
        navigation.viewControllers = [contactViewController]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func clickedAcount(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
        navigation.viewControllers = [contactViewController]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func clickedSearch(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller = appdelegate.window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let contactViewController = self.storyboard?.instantiateViewController(withIdentifier: "TeenViewController") as! TeenViewController
        contactViewController.objValue = "Search"
        navigation.viewControllers = [contactViewController]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    func addBanner() {
        
        bannerView.delegate = self
        bannerView.rootViewController = self
        //bannerView.adUnitID = "ca-app-pub-1863881439674210/1156077493"
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.load(GADRequest())
        let UDID = UIDevice.current.identifierForVendor?.uuidString

        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as! [String]
    }
    
    /// Tells the delegate an ad request loaded an ad.
       func adViewDidReceiveAd(_ bannerView: GADBannerView) {
           print("adViewDidReceiveAd")
       }
       
       /// Tells the delegate an ad request failed.
       func adView(_ bannerView: GADBannerView,
                   didFailToReceiveAdWithError error: GADRequestError) {
           print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
       }
       
       /// Tells the delegate that a full-screen view will be presented in response
       /// to the user clicking on an ad.
       func adViewWillPresentScreen(_ bannerView: GADBannerView) {
           print("adViewWillPresentScreen")
       }
       
       /// Tells the delegate that the full-screen view will be dismissed.
       func adViewWillDismissScreen(_ bannerView: GADBannerView) {
           print("adViewWillDismissScreen")
       }
       
       /// Tells the delegate that the full-screen view has been dismissed.
       func adViewDidDismissScreen(_ bannerView: GADBannerView) {
           print("adViewDidDismissScreen")
       }
       
       /// Tells the delegate that a user click will open another app (such as
       /// the App Store), backgrounding the current app.
       func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
           print("adViewWillLeaveApplication")
       }
}
