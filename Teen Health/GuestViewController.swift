//
//  GuestViewController.swift
//  Teen Health
//
//  Created by Gabani King on 17/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController


class GuestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func clickedGuest(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home: HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let homeNavigation = UINavigationController(rootViewController: home)
        let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
        controller.leftViewWidth = self.view.frame.size.width - 70
        
        homeNavigation.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = controller
        
        appDelegate.window?.makeKeyAndVisible()
    }
    
}
