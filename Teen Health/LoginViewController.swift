//
//  LoginViewController.swift
//  Teen Health
//
//  Created by Gabani King on 16/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import LGSideMenuController
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SVProgressHUD
import Toast_Swift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedSignin(_ sender: Any) {
        
        if self.isValidatedLogin() {
            LoginAccount()
        }
        
    }
    
    @IBAction func clickedForGot(_ sbender: Any) {
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if txtEmail.text == "" {
            self.view.makeToast("Enter Email Address")
            return false
        } else if AppUtilites.isValidEmail(testStr: (txtEmail.text)!) == false {
            self.view.makeToast("Enter Valid Email Address")
            return false
        } else if txtPass.text == "" {
            self.view.makeToast("Enter Password")
            return false
        }
        return true
    }
    
    func LoginAccount()
    {
        SVProgressHUD.show()
        
        Auth.auth().signIn(withEmail: self.txtEmail.text!, password: self.txtPass.text!) { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                
                SVProgressHUD.dismiss()
                let user = Auth.auth().currentUser
                
                let ref = Database.database().reference()
                
                ref.child("users").child(user!.uid).observeSingleEvent(of: .value) { (snapshot) in
                    
                    let userDic = snapshot.value as! NSDictionary
                    print(userDic)
                    
                    let firstName = userDic.value(forKey: "firstname") as? String
                    let lastname = userDic.value(forKey: "lastname") as? String
                    let dob = userDic.value(forKey: "dob") as? String
                    let email = userDic.value(forKey: "email") as? String
                    
                    UserDefaults.standard.setValue(firstName, forKey: "firstName")
                    UserDefaults.standard.setValue(lastname, forKey: "lastname")
                    UserDefaults.standard.setValue(dob, forKey: "dob")
                    UserDefaults.standard.setValue(email, forKey: "email")
                    UserDefaults.standard.synchronize()
                    
                }
                
                UserDefaults.standard.set(true, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let home: HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let homeNavigation = UINavigationController(rootViewController: home)
                let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                
                let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
                controller.leftViewWidth = self.view.frame.size.width - 70
                
                homeNavigation.navigationBar.isHidden = true
                appDelegate.window?.rootViewController = controller
                
                appDelegate.window?.makeKeyAndVisible()
                
            } else {
                
                SVProgressHUD.dismiss()
                self.view.makeToast(error?.localizedDescription)
                
            }
        }
    }
}


