//
//  SignUPViewController.swift
//  Teen Health
//
//  Created by Gabani King on 16/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SVProgressHUD
import LGSideMenuController

class SignUPViewController: UIViewController {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if txtFirstName.text == "" {
            self.view.makeToast("Enter your First name")
            return false
        } else if txtLastName.text == "" {
            self.view.makeToast("Enter your Last name")
            return false
        } else if txtDOB.text == "" {
            self.view.makeToast("Enter your DOB")
            return false
        } else if txtEmail.text == "" {
            self.view.makeToast("Enter your Email address")
            return false
        } else if AppUtilites.isValidEmail(testStr: (txtEmail.text)!) == false {
            self.view.makeToast("Enter Valid Email Address")
            return false
        } else if txtPass.text == "" {
            self.view.makeToast("Enter Password")
            return false
        }
        return true
    }
    
    @IBAction func clickedDOB(_ sender: UITextField) {
        datePicker.datePickerMode = .date
        
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "IST")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -10
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -18
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate

        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "CANCEL", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        txtDOB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @IBAction func clickedSihnuo(_ sender: Any) {
        if isValidatedLogin() {
            CreateAccount()
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: LoginViewController.self) {
                    isExist = true
                    break
                }
            }
            if isExist == true {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
        
    }
    
    func CreateAccount()
    {
        SVProgressHUD.show()
        let ref = Database.database().reference()
        
        Auth.auth().createUser(withEmail: txtEmail.text!, password: txtPass.text!) { (user, error) in
            
            
            if error == nil {
                print("You have successfully signed up")
                //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                
                let userData = ["id":user?.user.uid,"firstname": self.txtFirstName.text!,"lastname":self.txtLastName.text!,"dob":self.txtDOB.text!,"email":self.txtEmail.text!] as [String : Any]
                
                ref.child("users").child((user?.user.uid)!).setValue(userData)
                
                self.LoginAccount()
                
                
            } else {
                SVProgressHUD.dismiss()
                self.view.makeToast(error?.localizedDescription)
            }
        }
    }
    
    func LoginAccount()
    {
        
        Auth.auth().signIn(withEmail: self.txtEmail.text!, password: self.txtPass.text!) { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                
                SVProgressHUD.dismiss()
                let user = Auth.auth().currentUser
                
                let ref = Database.database().reference()
                
                ref.child("users").child(user!.uid).observeSingleEvent(of: .value) { (snapshot) in
                    
                    let userDic = snapshot.value as! NSDictionary
                    print(userDic)
                    
                    let firstName = userDic.value(forKey: "firstname") as? String
                    let lastname = userDic.value(forKey: "lastname") as? String
                    let dob = userDic.value(forKey: "dob") as? String
                    let email = userDic.value(forKey: "email") as? String
                    
                    UserDefaults.standard.setValue(firstName, forKey: "firstName")
                    UserDefaults.standard.setValue(lastname, forKey: "lastname")
                    UserDefaults.standard.setValue(dob, forKey: "dob")
                    UserDefaults.standard.setValue(email, forKey: "email")
                    UserDefaults.standard.synchronize()
                }
                
                UserDefaults.standard.set(true, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let home: HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let homeNavigation = UINavigationController(rootViewController: home)
                let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                
                let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
                controller.leftViewWidth = self.view.frame.size.width - 70
                
                homeNavigation.navigationBar.isHidden = true
                appDelegate.window?.rootViewController = controller
                
                appDelegate.window?.makeKeyAndVisible()
                
            } else {
                
                SVProgressHUD.dismiss()
                self.view.makeToast(error?.localizedDescription)
                
            }
        }
    }
    
}

