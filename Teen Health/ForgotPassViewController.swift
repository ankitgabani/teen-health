//
//  ForgotPassViewController.swift
//  Teen Health
//
//  Created by Gabani King on 18/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD

class ForgotPassViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedSend(_ sender: Any) {
        
        if txtEmail.text == "" {
            self.view.makeToast("Enter Valid Email Address")
        } else {

            SVProgressHUD.show()
            Auth.auth().sendPasswordReset(withEmail: txtEmail.text!) { error in
               
                SVProgressHUD.dismiss()
                
                if error != nil {
                    self.view.makeToast(error?.localizedDescription)

                } else {
                    self.view.makeToast("Reset email sent successfully, please Check your email")

                }
                
            }

        }
        
        
    }
    
    
    @IBAction func clickedSignin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
