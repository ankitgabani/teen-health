//
//  AppDelegate.swift
//  Teen Health
//
//  Created by Gabani King on 16/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Firebase
import LGSideMenuController
import GoogleMobileAds
import IQKeyboardManager
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        IQKeyboardManager.shared().isEnabled = true
        
        if let loginSuccess = UserDefaults.standard.value(forKey: "UserLogin") as? Bool {
            
            if loginSuccess == true {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let home: HomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let homeNavigation = UINavigationController(rootViewController: home)
                let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                
                let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
                controller.leftViewWidth = home.view.frame.size.width - 70
                
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = controller
                self.window?.makeKeyAndVisible()
            } else {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
                
                let homeNavigation = UINavigationController(rootViewController: home)
                let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                
                let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
                controller.leftViewWidth = home.view.frame.size.width - 70
                
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = controller
                self.window?.makeKeyAndVisible()
            }
            
        } else {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
            
            let homeNavigation = UINavigationController(rootViewController: home)
            let leftViewController: SideMenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
            controller.leftViewWidth = home.view.frame.size.width - 70
            
            homeNavigation.navigationBar.isHidden = true
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    
}

