//
//  TeenViewController.swift
//  Teen Health
//
//  Created by Gabani King on 17/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import WebKit
import SVProgressHUD
import GoogleMobileAds
import LGSideMenuController

class TeenViewController: UIViewController,WKNavigationDelegate,GADBannerViewDelegate {
    
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    var webView : WKWebView!
    
    var objValue = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBanner()
        
        webView = WKWebView()
        DispatchQueue.main.async {
            self.webView.frame = CGRect.init(x: 0, y: 0, width: self.mainView.frame.size.width, height: self.mainView.frame.size.height)
            
            if self.objValue == "Teens" {
                self.webView.load(NSURLRequest(url: NSURL(string: "https://hifajewellery.com/teens/")! as URL) as URLRequest)
            } else if self.objValue == "Kids" {
                self.webView.load(NSURLRequest(url: NSURL(string: "https://hifajewellery.com/kids/")! as URL) as URLRequest)

            } else if self.objValue == "Checker" {
                self.webView.load(NSURLRequest(url: NSURL(string: "https://hifajewellery.com/symptom-checker/")! as URL) as URLRequest)

            } else if self.objValue == "Food" {
                self.webView.load(NSURLRequest(url: NSURL(string: "https://hifajewellery.com/food-guide/")! as URL) as URLRequest)

            } else if self.objValue == "Search" {
                self.webView.load(NSURLRequest(url: NSURL(string: "https://hifajewellery.com/search/")! as URL) as URLRequest)
            }
            
            self.webView.allowsBackForwardNavigationGestures = true
            
            self.webView.navigationDelegate = self
            
            self.webView.scrollView.bounces = false
            
            self.mainView.addSubview(self.webView)
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
        
    }
 
    
    func addBanner() {
        
        bannerView.delegate = self
        bannerView.rootViewController = self
        //bannerView.adUnitID = "ca-app-pub-1863881439674210/1156077493"
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.load(GADRequest())
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as! [String]
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        if self.objValue == "Teens" || self.objValue == "Kids" {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.showLeftViewAnimated(self)
        }
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                
                UserDefaults.standard.setValue("", forKey: "firstName")
                UserDefaults.standard.setValue("", forKey: "lastname")
                UserDefaults.standard.setValue("", forKey: "dob")
                UserDefaults.standard.setValue("", forKey: "email")
                UserDefaults.standard.set(false, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.window?.rootViewController = homeNavigation
                    appDelegate.window?.makeKeyAndVisible()
                }
                
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } else {
            UserDefaults.standard.setValue("", forKey: "firstName")
                          UserDefaults.standard.setValue("", forKey: "lastname")
                          UserDefaults.standard.setValue("", forKey: "dob")
                          UserDefaults.standard.setValue("", forKey: "email")
                          UserDefaults.standard.set(false, forKey: "UserLogin")
                          UserDefaults.standard.synchronize()
                          
                          let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                          let home: GuestViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
                          let homeNavigation = UINavigationController(rootViewController: home)
                          homeNavigation.navigationBar.isHidden = true
                          if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                              appDelegate.window?.rootViewController = homeNavigation
                              appDelegate.window?.makeKeyAndVisible()
                          }
        }
    }
    
    
}
